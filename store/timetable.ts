import { ApiStatus } from "~/api/api.constants"
import timetable from '~/services/timetable.service'
import { ISolveArgs } from "~/utils/timetable/interfaces"
import { IForm, IStream, ITeacher } from "./school"

interface ITimetableState {
  loadingApiStatus: ApiStatus,

  // error
  error: any,
  errorMsg: string,

  timetable: any,
}

export interface ITimetableGenPayload {
  streams: IStream[],
  forms: IForm[],
  teachers: ITeacher[],
  daysCount: string,
  sessionsCount: string,

  options?: ISolveArgs,
}

const initialState: () => ITimetableState = () => ({
  loadingApiStatus: ApiStatus.Default,

  // errors
  error: null,
  errorMsg: '',

  timetable: null,
})

export const state = initialState

export const getters = {
  loadingApiStatus: (state: ITimetableState) => state.loadingApiStatus,
  error: (state: ITimetableState) => state.error,
  errorMsg: (state: ITimetableState) => state.errorMsg,

  timetable: (state: ITimetableState) => state.timetable,
}

export const actions = {
  generate: async ({ commit }: any, payload: ITimetableGenPayload) => {
    try {
      commit('generateRequest')

      const response = await timetable.generate(payload)
      console.log('Response: ', response.data.data)
      commit('generateSuccess', response.data.data)
    } catch (error) {
      window.e = error
      commit('generateError', error)
    }
  }
}
export const mutations = {
  generateRequest(state: ITimetableState) {
    state.loadingApiStatus = ApiStatus.Loading

    // resets
    state.error = null
    state.errorMsg = ''
  },
  generateSuccess(state: ITimetableState, timetable: any) {
    state.loadingApiStatus = ApiStatus.Success
    state.timetable = timetable
  },
  generateError(state: ITimetableState, error: any) {
    console.error(error)
    state.loadingApiStatus = ApiStatus.Error
    
    if (error.data) {
      // response error
      error = error.data

      state.error = error.data
      state.errorMsg = error.message
    }
  }
}