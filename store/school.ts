import { ApiStatus } from "~/api/api.constants";
import school from "~/services/school.service";
export interface IStream {
  name: string,
}

export interface IForm {
  name: string,
  streams: IStream[]
}

export interface ISubject {
  name: string
}
export interface ITeacher {
  name: string,
  forms: IForm[],
  subjects: ISubject[],
}

export interface ISchool {
  name: string,
  email: string,
  forms: IForm[],
  teachers: ITeacher[],
  streams: IStream[],
}

export interface ISchoolCreatePayload {
  name: string,
  email: string,

  // for debug
  formsCount?: number,
  streamsCount?: number,
  teachersCount?: number,
}

interface ISchoolState {
  loadingApiStatus: ApiStatus,
  createLoadingApiStatus: ApiStatus,

  // School Data
  school: ISchool | null,
}

export const initialState: () => ISchoolState = () => ({
  loadingApiStatus: ApiStatus.Default,
  createLoadingApiStatus: ApiStatus.Default,

  // School Data
  school: null
})

export const state = initialState

export const getters = {
  loadingApiStatus(state: ISchoolState) { return state.loadingApiStatus },
  createLoadingApiStatus(state: ISchoolState) { return state.createLoadingApiStatus },
  school: (state: ISchoolState) => state.school,
}

export const actions = {
  create: async ({ commit }: any, payload: ISchoolCreatePayload) =>{
    try {
      commit('schoolCreateRequest')
      
      const response = await school.create(payload)
      const school$ = response.data
      commit('schoolCreateSuccess', school$)
    } catch (error) {
      commit('schoolCreateError', error)
    }
  },
  details: async ({ commit }: any) => {
    try {
      commit('schoolRequest')

      const response = await school.details()
      commit('schoolSuccess', response.data)
    } catch (error) {
      commit('schoolError', error)
    }
  }
}

export const mutations = {
  schoolRequest(state: ISchoolState) {
    state.loadingApiStatus = ApiStatus.Loading
  },
  schoolSuccess(state: ISchoolState, school: ISchool) {
    state.loadingApiStatus = ApiStatus.Success
    // state.school = school
  },
  schoolError(state: ISchoolState, error: any) {
    state.loadingApiStatus = ApiStatus.Error
    console.error('Error: ', error)
  },

  schoolCreateRequest(state: ISchoolState) {
    state.createLoadingApiStatus = ApiStatus.Loading
  },
  schoolCreateSuccess(state: ISchoolState, school: ISchool) {
    state.createLoadingApiStatus = ApiStatus.Success
    state.school = school
  },
  schoolCreateError(state: ISchoolState, error: any) {
    state.createLoadingApiStatus = ApiStatus.Error
    console.error('Error: ', error)
  }
}