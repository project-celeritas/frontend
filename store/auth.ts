import { ApiStatus } from '~/api/api.constants';
import { IAuthState, ILoginPayload, ISignupPayload } from '~/utils/auth/auth.interfaces'
import authService from '~/services/auth.service';

export const initialState: () => IAuthState = () => ({
  token: '',
  loadingApiStatus: ApiStatus.Default,

  error: null,
  errorMsg: ''
})

export const state = initialState;

export const getters = {
  loadingApiStatus: (state: IAuthState) => state.loadingApiStatus,

  error: (state: IAuthState) => state.error,
  errorMsg: (state: IAuthState) => state.errorMsg,
}

export const actions = {
  login: async ({ commit }: any, payload: ILoginPayload) => {
    try {
      commit('loginRequest')

      const response = await authService.login(payload)
      const token = response.data.data
      commit('loginSuccess', token)
    }
    catch (error) {
      commit('loginError', error)
    }
  },
  signup: async ({ commit }: any, payload: ISignupPayload) => {
    try {
      commit('signupRequest')

      const response = await authService.signup(payload)
      commit('signupSuccess')
    }
    catch (error) {
      commit('signupError', error)
    }
  }
}

export const mutations = {
  // Login
  loginRequest(state: IAuthState) {
    state.loadingApiStatus = ApiStatus.Loading
    state.error = null
    state.errorMsg = ''
  },
  loginSuccess(state: IAuthState, token: string) {
    state.loadingApiStatus = ApiStatus.Success
  },
  loginError(state: IAuthState, error: any) {
    state.loadingApiStatus = ApiStatus.Error
    if (error.response.status === 401) {
      state.errorMsg = 'Incorrect email and/or password'
    }
    window.e = error
    console.log(error)
  },

  // Signup
  signupRequest(state: IAuthState) {
    state.loadingApiStatus = ApiStatus.Loading
  },
  signupSuccess(state: IAuthState) {
    state.loadingApiStatus = ApiStatus.Success
  },
  signupError(state: IAuthState, error: any) {
    state.loadingApiStatus = ApiStatus.Error
  }
}