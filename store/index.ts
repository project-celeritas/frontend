import { createLogger } from 'vuex'
const debug = process.env.NODE_ENV !== 'production'


const logger = (store: any) => {
    store.subscribe((mutation: any, state: any) => {
        console.log(mutation.name, mutation.payload)
        // console.log('State: ', state)
    })
}
export const plugins = debug ? [createLogger()] : []