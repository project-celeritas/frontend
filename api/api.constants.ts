export enum ApiStatus {
    Default = 0,
    Loading = 1,
    Success = 2,
    Error   = 3,
    Timeout = 4,
}