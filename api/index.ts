import axios, { Method } from 'axios';
import * as utils from "@/utils/auth";

declare global {
  interface Window {
    [propName: string]: any
  }
}

export const instance = axios.create({
  baseURL: process.env.NUXT_APP_API_URL || 'https://celeritas-dev.herokuapp.com/api/v1',
  timeout: 40000,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
});

// console.log(process.env.VUE_APP_API_URL);

instance.interceptors.request.use(config => {
  const accessToken = utils.getToken();
  if (accessToken) {
    // eslint-disable-next-line no-param-reassign
    config.headers.Authorization = `Bearer ${accessToken}`;
  }
  // console.log('Intercepted config: ', config)
  return config;
});

instance.interceptors.response.use(
  response => response,
  error => {
    window.apiError = error

    if (error.response) {
      window.responseError = error.response; // for debuging
      if (error.response.status === 401) {
        // TODO handle expired token
        // token expired or invalid
        // router.push({ name: "Login", params: { invalidToken: true } });
        // return Promise.reject(error);
      }
    }

    // other errors
    return Promise.reject(error);
  }
);

const api = {
  request(method: Method, url: string, data: any, success: any = null, error: any = null) {
    instance
      .request({
        url,
        data,
        method: method
      })
      .then(success)
      .catch(error);
  },

  get(url: string, success: any = null, error: any = null) {
    return this.request("get", url, {}, success, error);
  },

  post(url: string, data: any, success: any = null, error: any = null) {
    return this.request("post", url, data, success, error);
  },

  put(url: string, data: any, success: any = null, error: any = null) {
    return this.request("put", url, data, success, error);
  },

  patch(url: string, data: any, success: any = null, error: any = null) {
    return this.request("patch", url, data, success, error);
  },

  delete(url: string, data = {}, success: any = null, error: any = null) {
    return this.request("delete", url, data, success, error);
  }
};

window.apiInstance = instance;
window.env = process.env
export default api;
