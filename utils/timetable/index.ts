import { ITimetableGenPayload } from "~/store/timetable";
import { Grid, GridDay, GridEntry } from "./interfaces";
import { printDayGrid, printGrid, printTeacher, Solver } from "./utils";

export const EMPTY = '[E]'

export function generate({ forms, teachers, streams, daysCount, sessionsCount, options }: ITimetableGenPayload) {
  const daysCount$ = parseInt(daysCount);
  const sessionsCount$ = parseInt(sessionsCount);
  const formsCount = forms.length;
  const streamsCount = streams.length;

  // console.log(daysCount)

  // build timetable grid
  const grid: Grid = []
  for (let i = 0; i < daysCount$; ++i) {
    const gridDay: GridDay = []
    for (let i = 0; i < (formsCount * streamsCount); ++i) {
      const gridDayRow: GridEntry[] = []
      for (let i = 0; i < sessionsCount$; ++i) {
        gridDayRow.push(EMPTY)
      }

      gridDay.push(gridDayRow)
    }

    grid.push(gridDay)
  }

  // place the teachers
  // instantiate the solve
  const solver = new Solver(teachers, forms, streams)

  let solved = false
  let responseMessage: string | null = ''
  let responseData: any = null
  let solvedTable: Grid = []

  // const response: Record<string, any> = {}
  
  // teachers.forEach(teacher => printTeacher(teacher))
  for (let i = 0; i < daysCount$; ++i) {
    const dayGrid = grid[i]
    // console.log(`Day ${i + 1}...`)
    // printDayGrid(dayGrid)

    const { isValid, message, data } = solver.solve(dayGrid, options);
    // build response
    // response.error = !isValid;
    responseMessage = message
    responseData = data

    if (isValid) {
      solved = true
      solvedTable.push(dayGrid)
      // console.log('Solved:')
      // printDayGrid(dayGrid)
    } else {
      console.log(`Invalid: ${message}`)
      // printDayGrid(dayGrid)
      break
    }
  }

  console.log('\n')
  console.log('Solved?: ', solved, responseMessage)
  printGrid(solvedTable)

  return {
    error: !solved,
    message: responseMessage,
    data: solved ? solvedTable : responseData
  }
}