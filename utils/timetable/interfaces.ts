import { EMPTY } from '.'
import { ISubject, ITeacher } from "~/store/school";

export interface ISolveArgs {
  checkConsecutive?: boolean,
  consecutiveLimit?: number,
}

export interface ISolveResult {
  isValid: boolean,
  message: string | null,
  data?: any
}

export interface ISolveData {
  [propName: string]: any
}

export interface IGridPosition {
  row: number,
  col: number,
}

export interface IGridEntry {
  teacher: ITeacher,
  subject: ISubject
}

export type GridEntry = IGridEntry | typeof EMPTY
export type GridDay = GridEntry[][]
export type Grid = GridDay[]