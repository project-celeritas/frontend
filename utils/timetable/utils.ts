import { IForm, IStream, ISubject, ITeacher } from "~/store/school"
import { EMPTY } from "."
import {
  Grid, GridDay, GridEntry, IGridEntry, IGridPosition, ISolveArgs, ISolveResult
} from "./interfaces"

export function printGridRow(row: GridEntry[]) {
  const rowBuilt: any[] = []

  row.forEach(entry => {
    rowBuilt.push(
      entry === EMPTY ? entry : `${entry.teacher.name}-${entry.subject.name}`
    )
  })

  return rowBuilt
}

export function printDayGrid(grid: GridDay) {
  let gridBuiltStr = ''
  grid.forEach(row => gridBuiltStr += printGridRow(row) + '\n')
  console.log(gridBuiltStr)
}

export function printGrid(grid: Grid) {
  grid.forEach((day, index) => {
    console.log(`Day: ${index + 1}`)
    printDayGrid(day)
  })
}

export function printTeacher(teacher: ITeacher) {
  let forms = teacher.forms.map(form => form.name)
  let subjects = teacher.subjects.map(subject => subject.name)

  const output = `${teacher.name}\nForms\t\t-> ${forms}\nSubjects\t-> ${subjects}`

  console.log(output)
}


export class Solver {
  constructor(
    public teachers: ITeacher[],
    public forms: IForm[],
    public streams: IStream[],
  ) { }

  /* ------------------------ Utility Methods ------------------------ */
  private shuffle(array: any[]): any[] {
    /*
    Adapted from the first answer in
    https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
    */
    let currentIndex = array.length
    let randomIndex: number

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {
      // Pick a remaining element...
      randomIndex = Math.floor(Math.random() * currentIndex);
      currentIndex--;

      // And swap it with the current element.
      [array[currentIndex], array[randomIndex]] = [
        array[randomIndex], array[currentIndex]];
    }

    return array;
  }

  private findEmptySpot(grid: GridDay): IGridPosition | boolean {
    for (const row in grid) {
      for (const col in grid[row]) {
        const entry = grid[row][col]

        if (entry === EMPTY) {
          return { row: +row, col: +col }
        }
      }
    }

    return false
  }

  private preValidation(): ISolveResult {
    /*
    Performs pre-validation checks
    Checks:
      - Minimum number of teachers needed for the whole school
    */

    const teachersTotal = this.teachers.length
    const teachersMin = this.forms.length * this.streams.length
    if (teachersTotal < teachersMin)
      return {
        isValid: false,
        message: 'Insufficient number of teachers',
        data: ['We have more classes than teachers']
      }


    // Check if the teachers available can comfortable teach all the forms
    const formsMap: Record<string, any> = {}
    // Build formMap with streams
    for (const form of this.forms) {
      const form_key = form.name
      if (formsMap[form_key])
        formsMap[form_key].streamsCount = form.streams.length
      else
        formsMap[form_key] = {
          streamsCount: form.streams.length,
          teachersCount: 0
        }
    }
    // Update formMap's teachersCount
    for (const teacher of this.teachers) {
      for (const form of teacher.forms) {
        const form_key = form.name
        formsMap[form_key].teachersCount += 1
      }
    }
    // Do the check
    let hasError = false
    let errorData = []
    for (const formKey in formsMap) {
      const formData = formsMap[formKey]
      if (formData.streamsCount > formData.teachersCount) {
        hasError = true
        errorData.push(`Form ${formKey} has insufficient number of teachers`)
      }
    }
    if (hasError)
      return {
        isValid: false,
        message: 'Insufficient number of teachers',
        data: errorData
      }

    // console.log(formsMap)
    // throw new Error("Error");

    return { isValid: true, message: null }
  }

  private isTeacherValidInSpot(
    grid: GridDay,
    teacher: ITeacher,
    spot: IGridPosition,
    options: ISolveArgs = {}): boolean {
    const { row: spotRow, col: spotCol } = spot

    // Check column
    for (const rowIndex in grid) {
      let entry = grid[rowIndex][spotCol]
      // console.log(entry.name, teacher.name, entry === teacher, entry == teacher)
      if (+rowIndex !== spotRow && (entry as IGridEntry).teacher === teacher) {
        // console.log(`No fit: ${teacher.name} at: [${spotRow},${spotCol}]`)
        return false
      }
    }

    // Check if teacher can teach the form
    const formIndex = Math.floor(spotRow / this.streams.length)
    const form = this.forms[formIndex]
    if (!teacher.forms.includes(form)) {
      return false
    }

    // check for consecutivity
    const checkConsecutive = options.checkConsecutive ?? true
    if (checkConsecutive) {
      const consecutiveLimit = options.consecutiveLimit ?? 2
      let repetitionAmount = 0
      for (const entity of grid[spotRow]) {
        if ((entity as IGridEntry).teacher === teacher) {
          repetitionAmount++

          if (repetitionAmount >= consecutiveLimit)
            return false
        }
        else
          repetitionAmount = 0
      }
    }

    return true
  }

  private validateTeacherInSpot(
    grid: GridDay,
    teacher: ITeacher,
    spot: IGridPosition,
    options: ISolveArgs = {}): ISubject | boolean {
    const isValid = this.isTeacherValidInSpot(grid, teacher, spot, options)

    if (!isValid) return false

    // pick a subject
    const subjectsLength = teacher.subjects.length
    const subjectIndex = Math.floor(Math.random() * subjectsLength)
    const subject = teacher.subjects[subjectIndex]

    // console.log(teacher.subjects)
    return subject
  }
  /* ---------------------- End Utility Methods ---------------------- */

  solve(grid: GridDay, options: ISolveArgs = {}): ISolveResult {
    const solveInner = (): boolean => {
      // console.log('Grid')
      // printDayGrid(grid)
      // console.log('\n')

      const emptySpot = this.findEmptySpot(grid)

      // Grid has been fully filled
      if (emptySpot === false) {
        return true
      }

      const spot = emptySpot as IGridPosition
      const { row, col } = spot

      for (const teacher of this.teachers) {
        let subject = this.validateTeacherInSpot(grid, teacher, spot, options)
        if (subject) {
          grid[row][col] = {
            teacher,
            subject: subject as ISubject
          }

          // console.log('grid: ', grid[row], row)
          // return true

          if (solveInner())
            return true

          grid[row][col] = EMPTY
        }
      }

      return false
    }

    const { isValid, message, data } = this.preValidation()
    if (isValid) {
      const valid = solveInner()
      if (valid) {
        // Shuffle the teachers array so as to give different results each time
        // the solve method is run
        this.shuffle(this.teachers)
        return { isValid: true, message: null }
      }

      return { isValid: false, message: 'Invalid grid', data }
    }

    return { isValid: false, message, data }
  }
}