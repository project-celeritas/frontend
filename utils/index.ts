import { ValidationRule } from "./auth/auth.interfaces";

export const validationRules: Record<string, ValidationRule[]> = {
	consecutiveLimit: [
		v => {
			const val = +v
			if (val < 0)
				return 'Consecutive Limit must be positive'

			return true
		}
	],
	days: [
		v => !!v || 'Days cannot be empty',
		v => {
			const val = +v
			if (val < 1)
				return 'Days must be above 0'
			if (val > 7)
				return 'Days must be below 8'

			return true
		}
	],
	sessions: [
		v => !!v || 'Sessions cannot be empty',
		v => {
			const val = +v
			if (val < 1)
				return 'Sessions must be above 0'
			if (val > 20)
				return 'Sessions must be below 21'

			return true
		}
	]
}