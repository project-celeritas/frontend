import { ValidationRule } from './auth.interfaces'

// validation rules
export const validationRules: Record<string, ValidationRule[]> = {
    email: [v => !!v || 'Email is required', v => /^\w+@(\w+\.\w+)+$/.test(v) || 'Incorrect email format'],
    password: [v => !!v || 'Password is required'],
    schoolName: [v => !!v || 'School Name is required']
}

// token
export function getToken(): string {
    return ''
}
export function setToken(token: string) {}