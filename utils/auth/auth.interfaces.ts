import { ApiStatus } from "~/api/api.constants";

export interface ValidationRule {
  (value: string): boolean | string
}

export interface ILoginPayload {
  email: string,
  password: string,
}
export interface ISignupPayload {
  email: string,
  password: string,
  name?: string,
}

export interface IAuthState {
  token: string,
  loadingApiStatus: ApiStatus,

  error: any,
  errorMsg: string,
}