import { ISchool, IStream, IForm, ITeacher, ISubject, ISchoolCreatePayload } from '~/store/school'

export function generateStreams(streamsCount = 2): IStream[] {
	const streams: IStream[] = []
	for (let i = 0; i < streamsCount; ++i) {
		streams.push({
			name: `S${i + 1}`
		})
	}

	return streams
}

export function generateForms(streams: IStream[], formsCount = 3): IForm[] {
	const forms: IForm[] = []
	for (let i = 0; i < formsCount; ++i) {
		forms.push({
			name: `F${i + 1}`,
			streams: streams
		})
	}

	return forms
}

export function generateSubjects(subjectsCount = 5): ISubject[] {
	const subjects: ISubject[] = []
	for (let i = 0; i < subjectsCount; ++i) {
		subjects.push({
			name: `SUB${i + 1}`
		})
	}

	return subjects
}

export function generateSchoolData(payload: ISchoolCreatePayload, formsCount = 3, teachersCount: number | null = null, streamsCount: number = 2): ISchool {
	// const streamsCount = Math.round(Math.random() * 5);
	const streams = generateStreams(streamsCount)
	const forms = generateForms(streams, formsCount)

	const subjectsCount = 5
	const subjects = generateSubjects(subjectsCount)

	const formsLength = forms.length
	teachersCount = teachersCount ? teachersCount : (formsLength * streams.length)
	const teachers: ITeacher[] = []
	for (let i = 0; i < teachersCount; ++i) {
		const randomFormIndex1 = Math.floor(Math.random() * formsLength)
		const randomFormIndex2 = Math.floor(Math.random() * formsLength)
		const randomSubjectIndex1 = Math.floor(Math.random() * subjectsCount)

		teachers.push({
			name: `T${i + 1}`,
			forms: [forms[randomFormIndex1]],
			subjects: [subjects[randomSubjectIndex1]],
		})
	}
	
	return {
		name: payload.name,
		email: payload.email,
		forms,
		teachers,
		streams,
	}
}