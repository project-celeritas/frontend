import { ILoginPayload, ISignupPayload } from "~/utils/auth/auth.interfaces";
import api from "~/api";

const auth = {
  login(payload: ILoginPayload): Promise<any> {
    return new Promise((resolve, reject) => {
      api.post(
        "/auth/login",
        payload,
        (data: any) => resolve(data),
        (error: any) => reject(error)
      );
    });
  },
  signup(payload: ISignupPayload): Promise<any> {
    return new Promise((resolve, reject) => {
      api.post(
        "/auth/signup/",
        payload,
        (data: any) => resolve(data),
        (error: any) => reject(error)
      );
    });
  },
}

export default auth;