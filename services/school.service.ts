import api from '~/api';
import { ISchool, ISchoolCreatePayload, IStream } from '~/store/school';
import { generateSchoolData } from '~/utils/dummy'

const school = {
  create(payload: ISchoolCreatePayload): Promise<any> {
    return new Promise((resolve, reject) => {
      // api.post(
      //   '/schools/',
      //   payload,
      //   (data: any) => resolve(data),
      //   (error: any) => reject(error)
      // )
      try {
        const school = generateSchoolData(
          payload,
          payload.formsCount,
          payload.teachersCount,
          payload.streamsCount
        )
        
        resolve({ data: school })
      } catch (error) {
        reject(error)
      }
    })
  },
  details(): Promise<any> {
    return new Promise((resolve, reject) => {
      // api.get(
      //   '/school',
      //   (data: any) => resolve(data),
      //   (error: any) => reject(error)
      // )

      // mock data
      // try {
      //   const response: Record<string, ISchool> = {
      //     // data: generateSchoolData(4, 30, 5)
      //     data: {
      //       name: 'Super',
      //       streams: [{
      //         name: 'S1'
      //       }],
      //       forms: [],
      //       teachers: []
      //     }
      //   }

      //   resolve(response)
      // } catch (error) {
      //   reject(error)
      // }

      resolve({ data: {} })
    })
  }
}

export default school