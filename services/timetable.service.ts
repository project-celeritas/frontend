import { ITimetableGenPayload } from "~/store/timetable"
import { generate } from '~/utils/timetable'


const timetable = {
  generate(payload: ITimetableGenPayload): Promise<any> {
    return new Promise((resolve, reject) => {
      try {
        const response = {
          data: generate(payload)
        }

        if (response.data.error)
          reject(response)
        else
          resolve(response)
      } catch (error) {
        reject(error)
      }
    })
  }
}

export default timetable;